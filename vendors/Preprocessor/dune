;; Building the preprocessor as a library

(library
  (name Preprocessor)
  (public_name Preprocessor)
  (wrapped true)
  (libraries
    getopt
    simple-utils)
  (modules
    EvalOpt
    E_Parser
    E_Lexer
    E_AST
    Preproc)
  (preprocess
    (pps bisect_ppx --conditional))
  (flags (:standard -open Simple_utils)))

;; Building the lexers of the preprocessor

(ocamllex
   E_Lexer Preproc)

;; Building the parser of the preprocessor (for boolean expressions)

(menhir
  (modules E_Parser))

;; Building PreprocMain.exe for a standalone preprocessor

(executable
  (name PreprocMain)
  (modules PreprocMain)
  (libraries Preprocessor)
  (preprocess
    (pps bisect_ppx --conditional)))

;; Building E_LexerMain.exe for a standalone lexer of boolean
;; expressions

(executable
  (name E_LexerMain)
  (modules 
    E_LexerMain)
  (libraries Preprocessor)
  (preprocess
    (pps bisect_ppx --conditional)))

;; Building E_ParserMain.exe for a standalone parser of boolean
;; expressions

(executable
  (name E_ParserMain)
  (modules 
    E_ParserMain)
  (libraries Preprocessor)
  (preprocess
    (pps bisect_ppx --conditional)))
